
 fetch("http://127.0.0.1:5000/DATA/post.json")
.then(response => response.json())
.then(data => affichage(data.urlPost))
.catch(error => alert("Erreur : " + error));

var tabPost = []

function affichage(adresse) {

    adresse.forEach(element => {

        fetch(element)
        .then(response => response.json())
        .then(data => { 
            tabPost.push(data); 
            liste(tabPost);
        });
    });  
}

function trie(tab) {

    result = tab.sort((a, b) => a.date - b.date);
    result.reverse()
    return result
}

function liste(tabPost) {
    
    tabSort = trie(tabPost)

    var currentDiv = document.getElementById('postBox');
    
    // --------------------------------------------- Vide la Div
    while (currentDiv.firstChild) {
        currentDiv.removeChild(currentDiv.firstChild);
    }

    // --------------------------------------------- Création des élements
    tabSort.forEach(post => {

        var newDiv = document.createElement("div");
        newDiv.classList.add("post");

        var newP = document.createElement("p");
        var newContent = document.createTextNode(post.contenu);
        newP.appendChild(newContent);

        var newDivInfo = document.createElement("div");
        newDivInfo.classList.add("infoPost");

        var newSpan = document.createElement("span");
        var newDate = document.createTextNode(post.dateClean);
        newSpan.appendChild(newDate);

        currentDiv.appendChild(newDiv);

        newDiv.appendChild(newP);
        newDiv.appendChild(newDivInfo);
        newDivInfo.innerHTML = "<a href =" + post.url + "> Lien du Post ICI </a>";
        newDivInfo.appendChild(newSpan);

    })
}
