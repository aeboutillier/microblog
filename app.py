from flask import Flask, render_template, request, redirect, url_for, json, abort
import hashlib
from datetime import datetime

app = Flask(__name__, static_folder='DATA')


@app.route("/")
def index():

     return redirect(url_for('blog'))

@app.route("/blog")
def blog():

    return render_template('blog.html')

@app.errorhandler(403)
def error(e):

    return render_template('403.html'), 403

@app.route("/traitement", methods=['POST', 'GET'])
def traitement():


    if request.method == 'POST':

        # --------------------------------------------- Lire le secret
        with open('secret.txt', 'r') as secret_file:
            secret= secret_file.read()
        # --------------------------------------------- Données du Form
        dataForm = request.form
        contenu = dataForm.get('content')
        token = dataForm.get('token')
        tokenHash = hashlib.sha256(token.encode()).hexdigest()

        # --------------------------------------------- Comparaison du secret
        if tokenHash == secret :

            # --------------------------------------------- Génération de la Date
            now = datetime.now()
            date = now.strftime("%d%m%Y%H%M%S")
            dateClean = now.strftime("%d-%m-%Y %H:%M:%S")
            # --------------------------------------------- Construction de l'URL
            url = "http://127.0.0.1:5000/DATA/"+date+".json"
            # --------------------------------------------- Construction du Post
            newPost = {"url" : url,
            "contenu" : contenu,
            "date" : date,
            "dateClean" : dateClean}
            # --------------------------------------------- Création et Ecriture du Post
            with open('./DATA/'+ date +'.json', 'w') as json_file:
                json.dump(newPost, json_file)
            # --------------------------------------------- Ajout de l'URL du Post a la liste
            with open('./DATA/post.json', 'r') as json_file:
                liste = json.load(json_file)
                liste["urlPost"].append(url)
            with open('./DATA/post.json', 'w') as json_file:
                json.dump(liste, json_file)   

            return "Traitement des données", {"Refresh": "1; url=" + url_for('blog')}

        else : 
            abort(403);

    else : 
        return redirect(url_for('blog'))




if __name__ == "__main__":
    app.run(debug=True)